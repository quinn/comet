#pragma once
#include <stdint.h>
#include <string>
#include <vector>
#include <ctime>

#include "token.hpp"
#include "galaxyid.hpp"
#include "stx/request.hpp"


struct GogUser
{
    std::string username;
    std::string web_user_id;
    GalaxyID galaxy_user_id;
    std::string email;
    std::string avatar;
};

union FloatInt
{
    int32_t i;
    float f;
};

enum ValueType
{
    VALUETYPE_UNDEFINED,
    VALUETYPE_INT,
    VALUETYPE_FLOAT,
    VALUETYPE_NONE
};

struct GogUserStat
{
    uint64_t stat_id;
    std::string stat_key;
    ValueType stat_type;
    int window_size;
    bool increment_only;

    FloatInt value;
    FloatInt default_value;
    FloatInt min_value;
    FloatInt max_value;
    FloatInt max_change;
};

struct GogUserAchievement
{
    uint64_t achievement_id;
    std::string achievement_key;
    std::string name;
    std::string description;
    std::string image_url_locked;
    std::string image_url_unlocked;
    bool visible_while_locked;
    std::time_t unlock_time;
    float rarity;
    std::string rarity_desc;
    std::string rarity_slug;
};

struct GogUserAchievementList
{
    std::vector<GogUserAchievement> items;
    std::string mode;
    std::string language;
};

class GogApi
{
public:
    explicit GogApi(Token& token);

    GogUser getUser();
    std::vector<GogUserStat> getUserStats(GalaxyID user_id);
    GogUserAchievementList getUserAchievements(GalaxyID user_id);
    void setUserAchievement(GalaxyID user_id, uint64_t achievement_id, std::time_t unlock_time);

private:
    stx::HttpRequest defaultRequest(std::string url);

    Token& m_token;
};

class ApiError : public std::runtime_error
{
public:
    ApiError(std::string error_name_, std::string error_desc_);

    std::string error_name;
    std::string error_desc;
};


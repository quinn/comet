#pragma once
#include <stdint.h>


class GalaxyID
{
public:
    enum IDType
    {
        UNASSIGNED = 0,
        LOBBY = 1,
        USER = 2
    };

    GalaxyID()
    {

    }

    explicit GalaxyID(uint64_t combined)
    {
        m_combined = combined;
    }

    GalaxyID(IDType type, uint64_t base)
    {
        m_combined = ((uint64_t)(type) << 56) | base;
    }

    uint64_t combinedID()
    {
        return m_combined;
    }

    uint64_t baseID()
    {
        return m_combined & ((1ll << 56) - 1);
    }

    IDType type()
    {
        return (IDType)(m_combined >> 56);
    }

private:
    uint64_t m_combined;
};

#include <string>
#include <system_error>

#include "stx/path.hpp"
#include "stx/json.hpp"
#include "stx/socket.hpp"
#include "stx/logger.hpp"
#include "curl/curl.h"

#include "config.hpp"
#include "token.hpp"
#include "tokenassist.hpp"
#include "server.hpp"
#include "handler.hpp"



int main(int argc, char** argv)
{
    curl_global_init(CURL_GLOBAL_DEFAULT);
    stx::Logger logger("");
    stx::LoggerConfig& root_config = logger.getConfig();
    root_config.loglevel = stx::LOGLVL_INFO;

    std::string token_path = stx::expandHome(TOKEN_PATH);

    Token token;
    bool refreshed = false;
    if (argc >= 2 and std::string(argv[1]) == "create-token") {
        token = tokenAssistant();
        refreshed = true;
    } else {
        try {
            token = Token::fromFile(token_path);
        } catch (const std::system_error& e) {
            logger.error("Failed to open token: %", e.what());
            logger.info("Have you used 'cometd create-token' to create one?");
            return 1;
        } catch (const stx::json::exception& e) {
            logger.error("Malformed token: %", e.what());
            return 1;
        }
    }

    refreshed = refreshed || token.refreshExpired();
    if (refreshed) {
        try {
            token.save(token_path);
        } catch (const std::system_error& e) {
            logger.error("Failed to save token: %", e.what());
            return 1;
        }
    }

    stx::NetworkAddress addr(stx::makeIP4(127,0,0,1), 9977);

    GogServer server(addr, GogHandler::spawn, token);
    logger.info("Listening on %", addr);
    server.listenForever();

    curl_global_cleanup();
    return 0;
}

#include <string>
#include <sstream>
#include <iomanip>
#include <ctime>

#include "bytearray.hpp"



namespace stx {

bool startswith(char c, const std::string& str)
{
    if (str.empty()) {
        return false;
    }
    return str.front() == c;
}

bool startswith(const std::string& start, const std::string& str)
{
    return start == str.substr(0, start.size());
}

bool endswith(char c, const std::string& str)
{
    if (str.empty()) {
        return false;
    }
    return str.back() == c;
}

bool endswith(const std::string& end, const std::string& str)
{
    if (str.size() < end.size()) {
        return false;
    }
    return end == str.substr(str.size() - end.size(), end.size());
}

std::string hex(const ByteArray& ba)
{
    std::stringstream ss;
    ss << std::hex << std::setfill('0') << std::setw(2) << std::uppercase;
    for (char c : ba) {
        ss << (int)(unsigned char)c;
    }
    return ss.str();
}

std::tm parseDate(std::string input, const std::string& fmt)
{
    std::tm tmp;
    std::stringstream ss(input);
    ss >> std::get_time(&tmp, fmt.c_str());

    return tmp;
}

std::string formatDate(std::tm date, const std::string& fmt)
{
    std::stringstream ss;
    ss << std::put_time(&date, fmt.c_str());
    return ss.str();
}

}

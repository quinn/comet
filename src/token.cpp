#include <stdint.h>
#include <string>
#include <ctime>

#include "stx/bytearray.hpp"
#include "stx/request.hpp"
#include "stx/json.hpp"
#include "stx/stream.hpp"
#include "stx/serializer.hpp"
#include "stx/url.hpp"
#include "stx/string.hpp"
#include "curl/curl.h"

#include "token.hpp"
#include "config.hpp"
#include "api.hpp"


const char* URL_AUTH = "https://auth.gog.com/auth";
const char* URL_TOKEN = "https://auth.gog.com/token";

const char* REDIRECT_URL = "https://embed.gog.com/on_login_success?origin=client";



std::string getAuthUrl(std::string client_id)
{
    stx::Url auth_url(URL_AUTH);
    auth_url.addQuery("client_id", client_id);
    auth_url.addQuery("redirect_uri", REDIRECT_URL);
    auth_url.addQuery("response_type", "code");
    auth_url.addQuery("layout", "default");

    return auth_url.str();
}

static stx::HttpRequest defaultRequest(std::string url)
{
    stx::HttpRequest req;
    req.setUrl(url);
    req.setHeader("User-Agent", USER_AGENT);
    req.setDoThrow(true);
    return req;
}

static stx::HttpRequest defaultRequest(const stx::Url& url)
{
    return defaultRequest(url.str());
}



Token::Token(std::string client_id_, std::string client_secret_)
{
    client_id = client_id_;
    client_secret = client_secret_;
}

Token::Token(const Token& other)
{
    client_id = other.client_id;
    client_secret = other.client_secret;
    access_token = other.access_token;
    refresh_token = other.refresh_token;
    expires_in = other.expires_in;
    scope = other.scope;
    session_id = other.session_id;
    token_type = other.token_type;
    user_id = other.user_id;
    created = other.created;
}

Token& Token::operator=(const Token& other)
{
    m_refresh_lock.lock();
    m_refresh_lock.unlock();
    client_id = other.client_id;
    client_secret = other.client_secret;
    access_token = other.access_token;
    refresh_token = other.refresh_token;
    expires_in = other.expires_in;
    scope = other.scope;
    session_id = other.session_id;
    token_type = other.token_type;
    user_id = other.user_id;
    created = other.created;

    return *this;
}


Token Token::fromFile(std::string filename)
{
    Token token;
    token.load(filename);
    return token;
}

Token Token::fromCode(
    std::string login_code, std::string client_id_, std::string client_secret_)
{
    stx::Url token_url(URL_TOKEN);
    token_url.addQuery("client_id", client_id_);
    token_url.addQuery("client_secret", client_secret_);
    token_url.addQuery("grant_type", "authorization_code");
    token_url.addQuery("code", login_code);
    // Needed for origin verification
    token_url.addQuery("redirect_uri", REDIRECT_URL);

    stx::HttpRequest req = defaultRequest(token_url);
    stx::HttpResponse resp = req.request();
    Token token(client_id_, client_secret_);
    token.setData(stx::str(resp.getDataRef()));
    return token;
}

void Token::setData(const std::string& token_data)
{
    stx::json token_doc = stx::json::parse(token_data);

    if (token_doc.count("error")) {
        throw ApiError(
            token_doc["error"],
            token_doc["error_description"]
        );
    }

    if (token_doc.count("client_id")) {
        client_id = token_doc["client_id"];
    }
    if (token_doc.count("client_secret")) {
        client_secret = token_doc["client_secret"];
    }
    access_token = token_doc["access_token"];
    refresh_token = token_doc["refresh_token"];
    expires_in = token_doc["expires_in"];
    scope = token_doc["scope"];
    session_id = token_doc["session_id"];
    token_type = token_doc["token_type"];
    std::string user_id_str = token_doc["user_id"];
    user_id = GalaxyID(GalaxyID::USER, std::stoll(user_id_str));
    if (token_doc.count("created")) {
        created = token_doc["created"];
    } else {
        created = std::time(nullptr);
    }
}

std::string Token::getData()
{
    stx::json token_doc = stx::json::object();

    token_doc["client_id"] = client_id;
    token_doc["client_secret"] = client_secret;
    token_doc["access_token"] = access_token;
    token_doc["refresh_token"] = refresh_token;
    token_doc["expires_in"] = expires_in;
    token_doc["scope"] = scope;
    token_doc["session_id"] = session_id;
    token_doc["token_type"] = token_type;
    token_doc["user_id"] = std::to_string(user_id.baseID());
    token_doc["created"] = created;

    return token_doc.dump(4);
}

void Token::load(std::string filename)
{
    stx::FileStream file(filename, "r");
    stx::TextSerializer ser(file);

    setData(ser.readString(-1));
}

void Token::save(std::string filename)
{
    stx::FileStream file(filename, "w");
    stx::TextSerializer ser(file);

    ser.writeString(getData());
}

void Token::refreshFrom(std::string refresh_token_)
{
    m_refresh_lock.lock();
    refresh_token = refresh_token_;
    doRefresh();
    m_refresh_lock.unlock();
}

bool Token::refreshExpired(int margin)
{
    m_refresh_lock.lock();
    std::time_t expires_at = created + expires_in;
    std::time_t now = std::time(nullptr);
    bool expired = expires_at - now < margin;
    if (expired) {
        doRefresh();
    }
    m_refresh_lock.unlock();
    return expired;
}

void Token::forceRefresh()
{
    m_refresh_lock.lock();
    doRefresh();
    m_refresh_lock.unlock();
}

void Token::doRefresh()
{
    stx::Url token_url(URL_TOKEN);
    token_url.addQuery("client_id", client_id);
    token_url.addQuery("client_secret", client_secret);
    token_url.addQuery("grant_type", "refresh_token");
    token_url.addQuery("refresh_token", refresh_token);

    stx::HttpRequest req = defaultRequest(token_url);
    stx::HttpResponse resp = req.request();

    setData(stx::str(resp.getDataRef()));
}

#pragma once
#include <stdint.h>
#include <string>
#include <mutex>

#include "stx/bytearray.hpp"

#include "galaxyid.hpp"


const char* const GALAXY_ID = "46899977096215655";
const char* const GALAXY_SECRET = "9d85c43b1482497dbbce61f6e4aa173a433796eeae2ca8c5f6129f2dc4de46d9";



std::string getAuthUrl(std::string client_id);

class Token
{
public:
    Token() {}
    Token(std::string client_id_, std::string client_secret_);
    Token(const Token& other);
    Token& operator=(const Token& other);

    static Token fromFile(std::string filename);
    static Token fromCode(
        std::string login_code, std::string client_id_, std::string client_secret_);
    void setData(const std::string& token_data);
    std::string getData();
    void load(std::string filename);
    void save(std::string filename);
    void refreshFrom(std::string refresh_token_);
    bool refreshExpired(int margin=60);
    void forceRefresh();
private:
    void doRefresh();

public:
    std::string client_id;
    std::string client_secret;
    std::string access_token;
    std::string refresh_token;
    int expires_in;
    std::string scope;
    std::string session_id;
    std::string token_type;
    GalaxyID user_id;
    std::time_t created;
private:
    std::mutex m_refresh_lock;
};
